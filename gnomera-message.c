/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gnomera-message.c - Routines for device arrival/removal/event notices.

   Copyright (C) 2000 Free Software Foundation, Inc.

   Gnomera is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Gnomera is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   $Id$
*/

#include <stdio.h>
#include <stdarg.h>
#include <gnome.h>

#include <gnomera-utility.h>

/**
 * gnomera_message_internal:
 * @title: Dialog window title
 * @fmt: argument format
 * @args: 
 * @args2: 
 * 
 * Displays GNOME message dialog.
 **/
static void
gnomera_message_internal (gchar *title, const gchar *fmt, va_list * args,
			  va_list * args2)
{
	static gchar *buf = NULL;
	static gint alloc = 0;
	GtkWidget *dialog_window = NULL;
	GtkWidget *vbox;
	GtkWidget *label;

	gint len;

	len = format_string_length_upper_bound (fmt, args);

	if (len >= alloc)
	  {
		  if (buf)
			  g_free (buf);
		  alloc = nearest_pow (MAX (len + 1, 1024));
		  buf = g_new (char, alloc);
	  }

	vsprintf (buf, fmt, *args2);

	dialog_window = gnome_dialog_new (title, GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
	vbox = GNOME_DIALOG (dialog_window)->vbox;

	label = gtk_label_new (buf);
	gtk_misc_set_padding (GTK_MISC (label), 10, 10);
	gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);
	gtk_widget_show (label);

	gnome_dialog_set_close (GNOME_DIALOG (dialog_window), TRUE);

	gtk_widget_show (dialog_window);
}

void
gnomera_message (gchar * title, const gchar * format, ...)
{
	va_list args, args2;
	va_start (args, format);
	va_start (args2, format);
	gnomera_message_internal (title, format, &args, &args2);
	va_end (args);
	va_end (args2);
}

void
gnomera_message_notice (const gchar * format, ...)
{
	va_list args, args2;
	va_start (args, format);
	va_start (args2, format);
	gnomera_message_internal (_("Notice"), format, &args, &args2);
	va_end (args);
	va_end (args2);
}

void
gnomera_message_status (const gchar * format, ...)
{
	va_list args, args2;
	va_start (args, format);
	va_start (args2, format);
	gnomera_message_internal (_("Status"), format, &args, &args2);
	va_end (args);
	va_end (args2);
}

void
gnomera_message_warning (const gchar * format, ...)
{
	va_list args, args2;
	va_start (args, format);
	va_start (args2, format);
	gnomera_message_internal (_("Warning"), format, &args, &args2);
	va_end (args);
	va_end (args2);
}

void
gnomera_message_error (const gchar * format, ...)
{
	va_list args, args2;
	va_start (args, format);
	va_start (args2, format);
	gnomera_message_internal (_("Error"), format, &args, &args2);
	va_end (args);
	va_end (args2);
}
