#include <gtk/gtk.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#include "gnomera-device-15740.h"

static void gnomera_device_15740_class_init (GnomeraDevice15740Class *klass);

static void
gnomera_device_15740_init (GnomeraDevice15740 *gd);

static void
gnomera_device_15740_finalize (GtkObject *object);

static GnomeraDeviceClass *parent_class = NULL;

GtkType
gnomera_device_15740_get_type (void)
{
	static GtkType ps_type = 0;

	if (!ps_type)
	{
		      GtkTypeInfo ps_info =
		      {
			      "GnomeraDevice15740",
			      sizeof (GnomeraDevice15740),
			      sizeof (GnomeraDevice15740Class),
			      (GtkClassInitFunc) gnomera_device_15740_class_init,
			      (GtkObjectInitFunc) gnomera_device_15740_init,
			      /* reserved_1 */ NULL,
			      /* reserved_2 */ NULL,
			      (GtkClassInitFunc) NULL,
		      };

		      ps_type = gtk_type_unique (gnomera_device_get_type (), &ps_info);
	}

	return ps_type;
}

static void
gnomera_device_15740_class_init (GnomeraDevice15740Class *class)
{
	GtkObjectClass *object_class;
	GnomeraDeviceClass *gd_class;

	object_class = (GtkObjectClass *)class;
	gd_class = (GnomeraDeviceClass *)class;

	parent_class = gtk_type_class (gnomera_device_get_type ());

	object_class->finalize = gnomera_device_15740_finalize;
}

static void
gnomera_device_15740_init (GnomeraDevice15740 *gd15740)
{
	gd15740->standard_version = 0;
	gd15740->vendor_extension_id = 0x00000000;
	gd15740->vendor_extension_version = 0x00000000;
	g_strdup(gd15740->vendor_extension_desc, "NULL");
	gd15740->functional_mode = 0x00000000;
	strcpy(gd15740->manufacturer, "NULL");
	strcpy(gd15740->model, "NULL");
	gd15740->device_version = 0;
	gd15740->serial_number = 0;
}

GnomeraDevice15740 *
gnomera_device_15740_new (GnomeraDevice *gd15740
			  const char *model,
			  const char *vendor)
{
	GnomeraDevice15740 *gd15740;
	GnomeraDevice *gd;
	
	gd15740 = gtk_type_new (gnomera_device_15740_get_type ());
	gd = GNOMERA_DEVICE_CONTEXT (gd15740);

	return gd15740;
}

static void
gnomera_device_15740_finalize (GtkObject *object)
{
	GnomeraDevice15740 *gd15740;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOMERA_IS_DEVICE_15740 (object));

	gd15740 = GNOMERA_DEVICE_15740 (object);

	g_free (gd15740->standard_version);
	g_free (gd15740->standard_version);
	g_free (gd15740->vendor_extension_id);
	g_free (gd15740->vendor_extension_version);
	g_free (gd15740->vendor_extension_desc);
	g_free (gd15740->functional_mode);
	g_free (gd15740->manufacturer);
	g_free (gd15740->model);
	g_free (gd15740->device_version);
	g_free (gd15740->serial_number);

	(* GTK_OBJECT_CLASS (parent_class)->finalize) (object);
}

	
	
			  
	
	
	

	

	
	
