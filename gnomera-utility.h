/* This file contains some code from dia and glib:
 * Here are the copyright notices:
 *
 * Dia -- an diagram creation/manipulation program
 * Copyright (C) 1998 Alexander Larsson
 *
 * GLIB - Library of useful routines for C programming
 * Copyright (C) 1995-1997  Peter Mattis, Spencer Kimball and Josh MacDonald
 */

#ifndef __GNOMERA_UTILITY_H__
#define __GNOMERA_UTILITY_H__

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <glib.h>

extern gint nearest_pow (int num);
extern gint format_string_length_upper_bound (const char* fmt,
					      va_list *args);
#ifndef HAVE_SNPRINTF
extern gint snprintf ( char *str, size_t n, const char *format, ... );
#endif

#endif /* __GNOMERA_UTILITY_H__ */

