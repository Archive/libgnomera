/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gnomera-result.c - routines for error handling.

   Copyright (C) 2000 Free Software Foundation

   Gnomera is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Gnomera is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   $Id$
*/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnomera-datatypes.h>

/* Temporary hack to markup strings */

#ifdef _
#warning "_ already defined"
#else
#define _(x) x
#endif

#ifdef N_
#warning "N_ already defined"
#else
#define N_(x) x
#endif

static gchar *result_strings[] = {
	/* GNOMERA_OK */                      N_("No error"),
	/* GNOMERA_ERROR_DEVICE_NOT_FOUND */  N_("Device not found"),
	/* GNOMERA_ERROR_GENERIC */           N_("Generic error"),
	/* GNOMERA_ERROR_INTERNAL */          N_("Internal error"),
	/* GNOMERA_ERROR_NOT_SUPPORTED */     N_("Unsupported operation"),
	/* GNOMERA_ERROR_IO */                N_("I/O error"),
	/* GNOMERA_ERROR_CORRUPT_DATA */      N_("Data corrupted"),
	/* GNOMERA_ERROR_UNKNOWN_FORMAT */    N_("Unknown object format"),
};

/* FIXME: error checking */
gchar *
gnomera_result_to_string (GnomeraResult error)
{
	return _(result_strings[error]);
}
