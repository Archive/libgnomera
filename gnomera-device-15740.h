#ifndef __GNOMERA_DEVICE_15740_H__
#define __GNOMERA_DEVICE_15740_H__

#include "gnomera-datatypes.h"
#include "gnomera-device.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GNOMERA_TYPE_DEVICE_15740     (gnomera_device_15740_get_type ())
#define GNOMERA_DEVICE_15740(obj)     (GTK_CHECK_CAST ((obj), GNOMERA_TYPE_DEVICE_15740, GnomeraDevice15740))
#define GNOMERA_DEVICE_15740_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOMERA_TYPE_DEVICE_15740, GnomeraDevice15740Class))
#define GNOMERA_IS_DEVICE_15740(obj)  (GTK_CHECK_TYPE((obj), GNOMERA_TYPE_DEVICE_15740))
#define GNOMERA_IS_DEVICE_15740_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOMERA_TYPE_DEVICE_15740))

typedef struct _GnomeraDevice15740        GnomeraDevice15740;
typedef struct _GnomeraDevice15740Class   GnomeraDevice15740Class;
typedef struct _GnomeraDevice15740Private GnomeraDevice15740Private;

typedef enum {
	GNOMERA_VENDOR_EASTMAN_KODAK   = 0x00000001,
	GNOMERA_VENDOR_SEIKO_EPSON     = 0x00000002,
	GNOMERA_VENDOR_AGILIENT        = 0x00000003,
	GNOMERA_VENDOR_POLAROID        = 0x00000004,
	GNOMERA_VENDOR_AGFA            = 0x00000005,
	GNOMERA_VENDOR_MICROSOFT       = 0x00000006
} GnomeraVendorExtensionID;

typedef enum {
	GNOMERA_MODEL_GENERIC       = 0x00000001,
	GNOMERA_MODEL_KODAK_DC4800  = 0x00000002,
} GnomeraDevice15740Model;

typedef enum {
	GNOMERA_GET_DEVICE_INFO           = 0x1001,
	GNOMERA_OPEN_SESSION              = 0x1002,
	GNOMERA_CLOSE_SESSION             = 0x1003,
	GNOMERA_GET_STORAGE_IDS           = 0x1004,
	GNOMERA_GET_STORAGE_INFO          = 0x1005,
	GNOMERA_GET_NUM_OBJECTS           = 0x1006,
	GNOMERA_GET_OBJECT_HANDLES        = 0x1007,
	GNOMERA_GET_OBJECT_INFO           = 0x1008,
	GNOMERA_GET_OBJECT                = 0x1009,
	GNOMERA_GET_THUMB                 = 0x100a,
	GNOMERA_DELETE_OBJECT             = 0x100b,
	GNOMERA_SEND_OBJECT_INFO          = 0x100c,
	GNOMERA_SEND_OBJECT               = 0x100d,
	GNOMERA_INITIATE_CAPTURE          = 0x100e,
	GNOMERA_FORMAT_STORE              = 0x100f,
	GNOMERA_RESET_DEVICE              = 0x1010,
	GNOMERA_SELF_TEST                 = 0x1011,
	GNOMERA_SET_OBJECT_PROTECTION     = 0x1012,
	GNOMERA_POWER_DOWN                = 0x1013,
	GNOMERA_GET_DEVICE_PROP_DESC      = 0x1014,
	GNOMERA_GET_DEVICE_PROP_VALUE     = 0x1015,
	GNOMERA_SET_DEVICE_PROP_VALUE     = 0x1016,
	GNOMERA_RESET_DEVICE_PROP_VALUE   = 0x1017,
	GNOMERA_TERMINATE_OPEN_CAPTURE    = 0x1018,
	GNOMERA_MOVE_OBJECT               = 0x1019,
	GNOMERA_COPY_OBJECT               = 0x101a,
	GNOMERA_GET_PARTIAL_OBJECT        = 0x101b,
	GNOMERA_INITIATE_OPEN_CAPTURE     = 0x101c
} GnomeraDevice15740peration;

struct _GnomeraDevice15740 {
	GnomeraDevice gd;
	guint16 standard_version;
	guint32 vendor_extension_id;
	guint16 vendor_extension_version;
	GString  vendor_extension_desc;
	guint16 functional_mode;
        OperationCodes operations;
	EventCodes events;
	DevicePropCodes properties;
	CaptureFormatCodes capture_formats;
        CaptureFormatCodes image_formats;
	GString manufacturer;
	GString model;
	GString device_version;
	GString serial_number;
};

struct _GnomeraDevice15740Class {
	GnomeraDeviceClass parent_class;
	
};

GtkType gnomera_device_15740_get_type (void);

GnomeraDevice15740 *
gnomera_device_15740_new (GnomeraDevice *gd15740,
			  const char *model,
			  const char *vendor);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GNOMERA_DEVICE_15740_H__ */


