/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gnomera-device.h - digital camera device object and type definitions.

   Copyright (C) 2000 Free Software Foundation, Inc.

   Gnomera is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Gnomera is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   $Id$
*/

#ifndef __GNOMERA_DEVICE_H__
#define __GNOMERA_DEVICE_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <gtk/gtk.h>
#include <gmodule.h>
#include <gnomera-datatypes.h>
#include <gnomera-response.h>
#include <stdarg.h>

#define GNOMERA_TYPE_DEVICE            (gnomera_device_get_type ())
#define GNOMERA_DEVICE(obj)            (GTK_CHECK_CAST ((obj), GNOMERA_TYPE_DEVICE, GnomeraDevice))
#define GNOMERA_DEVICE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOMERA_TYPE_DEVICE, GnomeraDeviceClass))
#define IS_GNOMERA_DEVICE(obj)         (GTK_CHECK_TYPE ((obj), GNOMERA_TYPE_DEVICE))
#define IS_GNOMERA_DEVICE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOMERA_TYPE_DEVICE))
#define GNOMERA_DEVICE_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), GNOMERA_TYPE_DEVICE, GnomeraDeviceClass))

typedef struct _GnomeraDevice        GnomeraDevice;
typedef struct _GnomeraDeviceClass   GnomeraDeviceClass;

struct _GnomeraDevice {
	GtkObject    object;
	GnomeraTransportType transport;
	GnomeraProtocolType protocol;

	gchar *portname;
	
	/* usb_device */
	
	int fd;	

	gchar *model;
	
	guint portspeed;

	guint in_handle;
	guint err_handle;
	
	GModule *module;

	GList* errors_head;
	gint is_open;
};

struct _GnomeraDeviceClass {
	GtkObjectClass parent_class;
	void              (*get_device_info)     (GnomeraDevice*);
	GnomeraResponse   (*open_session)        (GnomeraDevice*, SessionID session_id);
	GnomeraResponse   (*close_session)       (void);
	void              (*warning)             (GnomeraDevice*);
	void              (*error)               (GnomeraDevice*);
	void              (*interrupt)           (GnomeraDevice*);
};


GtkType gnomera_device_get_type (void);
GnomeraDevice*  gnomera_device_new (GnomeraTransportType transport,
				    GnomeraProtocolType  protocol,
				    gchar *portname);
void            gnomera_device_free (GnomeraDevice *gd);
gint            gnomera_device_get_device_info (GnomeraDevice *gd);
GnomeraResponse gnomera_device_open_session (SessionID session_id);
GnomeraResponse gnomera_device_close_session (void);

#define         gnomera_device_is_open (x) ((x) ? GNOMERA_DEVICE(x)->is_open : FALSE)

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GNOMERA_DEVICE_H__ */

