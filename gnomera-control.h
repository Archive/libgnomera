/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gnomera-control.h - Bonobo control for digital still camera devices.

   Copyright (C) 2000 Free Software Foundation, Inc.

   Gnomera is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Gnomera is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   $Id$
*/

#ifndef __GNOMERA_CONTROL_H__
#define __GNOMERA_CONTROL_H__

#include <gnomera-device.h>
#include <gnomera.h>
#include <bonobo.h>

BEGIN_GNOME_DECLS

#define GNOMERA_CONTROL_TYPE           (gnomera_control_get_type ())
#define GNOMERA_CONTROL(o)             (GTK_CHECK_CAST ((o), GNOMERA_CONTROL_TYPE, GnomeraControl))
#define GNOMERA_CONTROL_CLASS(k)       (GTK_CHECK_CLASS_CAST ((k), GNOMERA_CONTROL_TYPE, GnomeraControlClass))

#define GNOMERA_IS_CONTROL(o)          (GTK_CHECK_TYPE ((o), GNOMERA_CONTROL_TYPE))
#define GNOMERA_IS_CONTROL_CLASS(k)    (GTK_CHECK_CLASS_TYPE ((k), GNOMERA_CONTROL_TYPE))

typedef struct _GnomeraControl         GnomeraControl;
typedef struct _GnomeraControlClass    GnomeraControlClass;
typedef struct _GnomeraControlPrivate  GnomeraControlPrivate;

struct _GnomeraControl {
	BonoboControl control;
	GnomeraControlPrivate *priv;
};

struct _GnomeraControlClass {
	BonoboControlClass parent_class;
};

GnomeraControl *
gnomera_control_new                   (GnomeraDevice *device);

GtkType
gnomera_control_get_type              (void) G_GNUC_CONST;

Bonobo_Control
gnomera_control_corba_object_create   (BonoboObject  *object);

GnomeraControl
gnomera_control_construct             (GnomeraControl  *control,
				       Bonobo_Control   corba_object,
				       GnomeraDevice   *device);

END_GNOME_DECLS

#endif __GNOMERA_CONTROL_H__



