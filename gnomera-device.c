/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gnomera-device.c - digital camera device object and type definitions.

   Copyright (C) 2000 Free Software Foundation, Inc.

   Gnomera is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Gnomera is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   $Id$
*/

#include <glib.h>
#include <gtk/gtk.h>

#include <gnomera-device.h>

enum
{
	ERROR,
	WARNING,
	OPEN_SESSION,
	CLOSE_SESSION,
	GET_DEVICE_INFO,
	LAST_SIGNAL
};

static gint gnomera_device_signals[LAST_SIGNAL] = { 0, };
static void gnomera_device_class_init (GnomeraDeviceClass *klass);
static void gnomera_device_init (GnomeraDevice *gd);

static void
gnomera_device_real_error (GnomeraDevice* gd, GList *errors)
{
	g_print ("%s: %d: %s called\n", __FILE__, __LINE__, __PRETTY_FUNCTION__);
	gd->errors_head = g_list_concat (gd->errors_head, errors);
}

static void
gnomera_device_real_warning (GnomeraDevice *gd, GList *warnings)
{
	g_print ("%s: %d: %s called\n", __FILE__, __LINE__, __PRETTY_FUNCTION__);
}

GtkType
gnomera_device_get_type (void)
{
	static GtkType gnomera_device_type = 0;

	if (!gnomera_device_type)
	{
		GtkTypeInfo gnomera_device_info =
		{
			"GnomeraDevice",
			sizeof (GnomeraDevice),
			sizeof (GnomeraDeviceClass),
			(GtkClassInitFunc) gnomera_device_class_init,
			(GtkObjectInitFunc) gnomera_device_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};
		gnomera_device_type = gtk_type_unique (gtk_object_get_type (), &gnomera_device_info);
	}
	return gnomera_device_type;
}

static void
gnomera_device_class_init (GnomeraDeviceClass *klass)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) klass;

	gnomera_device_signals[ERROR] =
		gtk_signal_new ("error",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GnomeraDeviceClass, error),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	gnomera_device_signals[WARNING] =
		gtk_signal_new ("warning",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GnomeraDeviceClass, warning),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	gnomera_device_signals[OPEN_SESSION] =
		gtk_signal_new ("open_session",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GnomeraDeviceClass, open_session),
				gtk_signal_default_marshaller,
				GTK_TYPE_NONE, 0);

	gnomera_device_signals[OPEN_SESSION] =
		gtk_signal_new ("close_session",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GnomeraDeviceClass, close_session),
				gtk_signal_default_marshaller,
				GTK_TYPE_NONE, 0);

	gnomera_device_signals[GET_DEVICE_INFO] =
		gtk_signal_new ("get_device_info",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GnomeraDeviceClass, get_device_info),
				gtk_signal_default_marshaller,
				GTK_TYPE_NONE, 0);

	gtk_object_class_add_signals(object_class, gnomera_device_signals, LAST_SIGNAL);

/*  	klass->error           = gnomera_device_real_error; */
/*  	klass->warning         = gnomera_device_real_warning; */
/*    	klass->get_device_info = gnomera_device_get_device_info; */
/*    	klass->open_session    = gnomera_device_open_session;  */
/*    	klass->close_session   = gnomera_device_close_session; */
}

static void
gnomera_device_init (GnomeraDevice *gd)
{
	g_print ("%s: %d: %s called\n", __FILE__, __LINE__, __PRETTY_FUNCTION__);
	g_return_if_fail(IS_GNOMERA_DEVICE(gd));
}

/**
 * gnomera_device_new:
 * @transport: a supported transport type
 * @protocol: a supported protocol type
 * @portname: an I/O devicename
 *
 * This function allocated the memory for a device object.
 *
 * Return value: a pointer to the device object
 **/
GnomeraDevice *
gnomera_device_new (GnomeraTransportType transport,
		    GnomeraProtocolType  protocol,
		    gchar *portname)

{
	GnomeraDevice *gd;
	gd = GNOMERA_DEVICE (gtk_type_new(gnomera_device_get_type()));

	g_return_val_if_fail (transport != 0, NULL);
	g_return_val_if_fail (protocol != 0, NULL);
	g_return_val_if_fail (portname != NULL, NULL);

	gd->transport = transport;
	gd->protocol  = protocol;
	gd->portname  = portname;

	/* FIXME: Temporary!!! */
  	if (gd->transport == GNOMERA_TRANSPORT_USB) {
  		g_print ("Registering USB device...\n");
  	}
	if (gd->protocol == GNOMERA_PROTOCOL_PTP) {
		g_print ("Picture Transfer Protocol (ISO 15740) session...\n");
	}
	return (gd);
}


void
gnomera_device_free (GnomeraDevice *gd)
{
	g_return_if_fail (IS_GNOMERA_DEVICE (gd));
	g_free (gd);
}

gint
gnomera_device_get_device_info (GnomeraDevice *gd)
{
	GnomeraResponse response;
	g_return_val_if_fail (IS_GNOMERA_DEVICE(gd), -1);
	g_return_val_if_fail (gd->is_open != 0, -1);

	if (gd->is_open) {
		g_print ("Apparently connectivity with the device.\n");
	} else {
		response = gnomera_device_open_session (0x00000000);
		g_print ("%s\n", gnomera_response_to_string (response));
		gd->is_open = 1;
	}
	return GNOMERA_OK;
}

GnomeraResponse
gnomera_device_open_session                     (SessionID session_id)
{
	return (0x2000);
}

GnomeraResponse
gnomera_device_close_session                    (void)
{
	return (0x2000);
}



