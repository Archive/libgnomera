/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gnomera-datatypes.h - enums and constants.

   Copyright (C) 2000 Free Software Foundation

   Gnomera is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Gnomera is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   $Id$
*/

#ifndef __GNOMERA_DATATYPES_H__
#define __GNOMERA_DATATYPES_H__

#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <glib.h>
#include <wchar.h>

typedef guint16 OperationCode;
typedef guint16 ResponseCode;
typedef guint16 EventCode;
typedef guint16 DevicePropCode;
typedef guint16 ObjectFormatCode;
typedef guint32 StorageID;
typedef guint32 TransactionID;
typedef guint32 SessionID;
typedef guint32 ObjectHandle;

/* Basic enumerations.  */

/* IMPORTANT NOTICE: If you add error types here, please also add the
   corresponsing descriptions in `gnomera-result.c'.  Moreover, *always*
   add new values at the end of the list, and *never* remove values.  */

typedef enum {
	GNOMERA_OK                        = 0,
	GNOMERA_ERROR_DEVICE_NOT_FOUND    = 1 << 0,
	GNOMERA_ERROR_GENERIC             = 1 << 1,
	GNOMERA_ERROR_INTERNAL            = 1 << 2,
	GNOMERA_ERROR_NOT_SUPPORTED       = 1 << 3,
	GNOMERA_ERROR_IO                  = 1 << 4,
	GNOMERA_ERROR_CORRUPT_DATA        = 1 << 5,
	GNOMERA_ERROR_UNKNOWN_FORMAT      = 1 << 6,
	GNOMERA_ERROR_NUM_ERRORS
} GnomeraResult;


typedef enum {
	GNOMERA_TRANSPORT_NONE            = 0,
	GNOMERA_TRANSPORT_USB             = 1 << 0,
	GNOMERA_TRANSPORT_SERIAL          = 1 << 1,
	GNOMERA_TRANSPORT_NETWORK         = 1 << 2
} GnomeraTransportType;

typedef enum {
	GNOMERA_PROTOCOL_NONE             = 0,
	GNOMERA_PROTOCOL_PTP              = 1 << 0
} GnomeraProtocolType;

/* ISO 15740 standard object format codes */
typedef enum {
/* Ancillary data file formats */
	GNOMERA_OBJECT_UNDEFINED_NONIMAGE = 0x3000,
	GNOMERA_OBJECT_ASSOCIATION        = 0x3001,
	GNOMERA_OBJECT_SCRIPT             = 0x3002,
	GNOMERA_OBJECT_EXECUTABLE         = 0x3003,
	GNOMERA_OBJECT_TEXT               = 0x3004,
	GNOMERA_OBJECT_HTML               = 0x3005,
	GNOMERA_OBJECT_DPOF               = 0x3006,
	GNOMERA_OBJECT_AIFF               = 0x3007,
	GNOMERA_OBJECT_WAV                = 0x3008,
	GNOMERA_OBJECT_MP3                = 0x3009,
	GNOMERA_OBJECT_AVI                = 0x300a,
	GNOMERA_OBJECT_MPEG               = 0x300b,
	GNOMERA_OBJECT_ASF                = 0x300c,

/* Image file formats */

	GNOMERA_OBJECT_UNDEFINED_IMAGE    = 0x3800,
	GNOMERA_OBJECT_EXIF               = 0x3801,
	GNOMERA_OBJECT_TIFF_EP            = 0x3802,
	GNOMERA_OBJECT_FLASHPIX           = 0x3803,
	GNOMERA_OBJECT_BMP                = 0x3804,
	GNOMERA_OBJECT_CIFF               = 0x3805,
	GNOMERA_OBJECT_UNDEFINED_1        = 0x3806,
	GNOMERA_OBJECT_GIF                = 0x3807,
	GNOMERA_OBJECT_JFIF               = 0x3808,
	GNOMERA_OBJECT_PCD                = 0x3809,
	GNOMERA_OBJECT_PICT               = 0x380a,
	GNOMERA_OBJECT_PNG                = 0x380b,
	GNOMERA_OBJECT_UNDEFINED_2        = 0x380c,
	GNOMERA_OBJECT_TIFF               = 0x380d,
	GNOMERA_OBJECT_TIFF_IT            = 0x380e,
	GNOMERA_OBJECT_JP2                = 0x380f,
	GNOMERA_OBJECT_JPX                = 0x3810
} GnomeraObjectFormat;

typedef gchar AssociationDesc;
typedef guint8 AssociationCode;

typedef GArray OperationCodes;
typedef GArray EventCodes;
typedef GArray DevicePropCodes;
typedef GArray ObjectFormatCodes;
typedef GArray CaptureFormatCodes;

/* ISO 15740 Device event codes */
typedef enum {
	GNOMERA_EVENT_UNDEFINED               = 0x4000,
	GNOMERA_EVENT_CANCEL_TRANSACTION      = 0x4001,
	GNOMERA_EVENT_OBJECT_ADDED            = 0x4002,
	GNOMERA_EVENT_OBJECT_REMOVED          = 0x4003,
	GNOMERA_EVENT_STORE_ADDED             = 0x4004,
	GNOMERA_EVENT_STORE_REMOVED           = 0x4005,
	GNOMERA_EVENT_DEVICE_PROP_CHANGED     = 0x4006,
	GNOMERA_EVENT_OBJECT_INFO_CHANGED     = 0x4007,
	GNOMERA_EVENT_DEVICE_INFO_CHANGED     = 0x4008,
	GNOMERA_EVENT_REQUEST_OBJECT_TRANSFER = 0x4009,
	GNOMERA_EVENT_STORE_FULL              = 0x400a,
	GNOMERA_EVENT_DEVICE_RESET            = 0x400b,
	GNOMERA_EVENT_STORAGE_INFO            = 0x400c,
	GNOMERA_EVENT_CAPTURE_COMPLETE        = 0x400d,
	GNOMERA_EVENT_UNREPORTED_STATUS       = 0x400e
} GnomeraEventType;

/* ISO 15740 Device property codes */
typedef enum {
	GNOMERA_PROP_UNDEFINED                = 0x5000,
	GNOMERA_PROP_BATTERY_LEVEL            = 0x5001,
	GNOMERA_PROP_FUNCTIONAL_MODE          = 0x5002,
	GNOMERA_PROP_IMAGE_SIZE               = 0x5003,
	GNOMERA_PROP_COMPRESSION_SETTING      = 0x5004,
	GNOMERA_PROP_WHITE_BALANCE            = 0x5005,
	GNOMERA_PROP_RGB_GAIN                 = 0x5006,
	GNOMERA_PROP_F_NUMBER                 = 0x5007,
	GNOMERA_PROP_FOCAL_LENGTH             = 0x5008,
	GNOMERA_PROP_FOCUS_DISTANCE           = 0x5009,
	GNOMERA_PROP_FOCUS_MODE               = 0x500a,
	GNOMERA_PROP_EXPOSURE_METERING_MODE   = 0x500b,
	GNOMERA_PROP_FLASH_MODE               = 0x500c,
	GNOMERA_PROP_EXPOSURE_TIME            = 0x500d,
	GNOMERA_PROP_EXPOSURE_PROGRAM_MODE    = 0x500e,
	GNOMERA_PROP_EXPOSURE_INDEX           = 0x500f,
	GNOMERA_PROP_EXPOSUREBIASCOMPENSATION = 0x5010,
	GNOMERA_PROP_DATE_TIME                = 0x5011,
	GNOMERA_PROP_CAPTURE_DELAY            = 0x5012,
	GNOMERA_PROP_STILL_CAPTURE_MODE       = 0x5013,
	GNOMERA_PROP_CONTRAST                 = 0x5014,
	GNOMERA_PROP_SHARPNESS                = 0x5015,
	GNOMERA_PROP_DIGITAL_ZOOM             = 0x5016,
	GNOMERA_PROP_EFFECT_MODE              = 0x5017,
	GNOMERA_PROP_BURST_NUMBER             = 0x5018,
	GNOMERA_PROP_BURST_INTERVAL           = 0x5019,
	GNOMERA_PROP_TIMELAPSE_MODE           = 0x501a,
	GNOMERA_PROP_TIMELAPSE_INTERVAL       = 0x501b,
	GNOMERA_PROP_FOCUS_METERING_MODE      = 0x501c,
	GNOMERA_PROP_UPLOAD_URL               = 0x501d,
	GNOMERA_PROP_ARTIST                   = 0x501e,
	GNOMERA_PROP_COPYRIGHT_INFO           = 0x501f,
} GnomeraDevicePropType;

#endif /* __GNOMERA_DATATYPES_H__ */
