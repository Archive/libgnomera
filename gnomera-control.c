/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gnomera-control.c - Bonobo control for digital still camera devices.

   Copyright (C) 2000 Free Software Foundation, Inc.

   Gnomera is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Gnomera is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   $Id$
*/

#include <config.h>
#include <stdio.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkmarshal.h>
#include <gtk/gtktypeutils.h>

#include <gnome.h>
#include <gnomera-control.h>

struct _GnomeraControlPrivate {
	GnomeraDevice     *device;
/*  	GnomeraDeviceView *device_view; */

	BonoboPropertyBag *propertybag;
	GtkWidget         *main_window;
	BonoboUIComponent *uic;
};

POA_Bonobo_Control__vepv gnomera_control_vepv;

static BonoboControlClass *gnomera_control_parent_class;

static void
gnomera_control_set_ui_container   (GnomeraControl      *control,
				    Bonobo_UIContainer  ui_container);

static void
gnomera_control_unset_ui_container (GnomeraControl      *control);

static void
init_gnomera_control_corba_class (void)
{
	/* Set up vector of epvs */
	gnomera_control_vepv.Bonobo_Unknown_epv = bonobo_object_get_epv ();
	gnomera_control_vepv.Bonobo_Control_epv = bonobo_control_get_epv ();
}

static void
gnomera_control_activate (GnomeraControl *object, gboolean state)
{
	GnomeraControl *control;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOMERA_IS_CONTROL (object));

	control = GNOMERA_CONTROL (object);

	if (state) {
		Bonobo_UIContainer ui_container;
		ui_container = bonobo_control_get_remote_ui_container (BONOBO_CONTROL (control));
		if (ui_container != CORBA_OBJECT_NIL) {
			gnomera_control_set_ui_container (control, ui_container);
			bonobo_object_release_unref (ui_container, NULL);
		}
	} else {
		gnomera_control_unset_ui_contrainer (control);
	}

	if (BONOBO_CONTROL_CLASS (gnomera_control_parent_class)->activate)
		BONOBO_CONTROL_CLASS (gnomera_control_parent_class)->activate (object, state);
}

static void
gnomera_control_destroy (GtkObject *object)
{
	GnomeraControl *control;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOMERA_IS_CONTROL(object));

	control = GNOMERA_OBJECT (object);

	if (control->priv->property_bag) {
		bonobo_object_unref (BONOBO_OBJECT (control->priv->property_bag));
		control->priv->property_bag = NULL;
	}

	if (control->priv->main_window) {
		gtk_widget_unref (control->priv->main_window);
		control->priv->main_window = NULL;
	}

	if (control->priv->uic) {
		bonobo_object_unref (BONOBO_OBJECT (control->priv->uic));
		control->priv->uic = NULL;
	}

	GTK_OBJECT_CLASS (gnomera_control_parent_class)->destroy (object);
}

static void
gnomera_control_finalize (GtkObject *object)
{
	GnomeraControl *control;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOMERA_IS_CONTROL (object));

	control = GNOMERA_CONTROL (object);
	g_free (control->priv);

	GTK_OBJECT_CLASS (gnomera_control_parent_class)->finalize (object);
}

static void
gnomera_control_class_init (GnomeraControl *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *)klass;
	BonoboControlClass *control_class = (BonoboControlClass *)klass;
	gnomera_control_parent_class = gtk_type_class (bonobo_control_get_type ());

	object_class->destroy = gnomera_control_destroy;
	object_class->finalize = gnomera_control_finalize;

	control_class->activate = gnomera_control_activate;

	init_gnomera_control_corba_class ();
}

static void
gnomera_control_init (GnomeraControl *control)
{
	control->priv = g_new0 (GnomeraControlPrivate, 1);
}

GtkType
gnomera_control_get_type (void)
{
	static GtkType type = 0;

	if (!type){
		GtkTypeInfo info = {
			"GnomeraControl",
			sizeof (GnomeraControl),
			sizeof (GnomeraControlClass),
			(GtkClassInitFunc) gnomera_control_class_init,
			(GtkObjectInitFunc) gnomera_control_init,
			NULL, /* reserved 1 */
			NULL, /* reserved 2 */
                        (GtkClassInitFunc) NULL
		};
		
		type = gtk_type_unique (bonobo_control_get_type (), &info);
	}
	
	return type;
}

Bonobo_Control
gnomera_control_corba_object_create (BonoboObject *object)
{
	POA_Bonobo_Control *servant;
	CORBA_Environment ev;

	servant = (POA_Bonobo_Control *) g_new0 (BonoboObjectServant, 1);
	servant->vepv = &gnomera_control_vepv;

	CORBA_exception_init (&ev);
	POA_Bonobo_Control__init ((PortableServer_Servant) servant, &ev);
	if (ev._major != CORBA_NO_EXCEPTION){
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}

	CORBA_exception_free (&ev);
	return (Bonobo_Control) bonobo_object_activate_servant (object, servant);
}

GnomeraControl *
gnomera_control_construct (GnomeraControl *control, Bonobo_Control corba_object,
			   GnomeraDevice *device)
{
        BonoboControl *retval;

	g_return_val_if_fail (control != NULL, NULL);
	g_return_val_if_fail (GNOMERA_IS_CONTROL (control), NULL);
	g_return_val_if_fail (corba_object != CORBA_OBJECT_NIL, NULL);
	g_return_val_if_fail (device != NULL, NULL);
	g_return_val_if_fail (GNOMERA_IS_DEVICE (device), NULL);
        control->priv->device = device;
	bonobo_object_ref (BONOBO_OBJECT (device));
}

GnomeraControl *
gnomera_control_new (GnomeraDevice *device)
{
	GnomeraControl *control;
	Bonobo_Control corba_object;

	g_return_val_if_fail (device != NULL, NULL);
	g_return_val_if_fail (GNOMERA_IS_DEVICE (device), NULL);

	control = gtk_type_new (gnomera_control_get_type ());

	corba_object = gnomera_control_corba_object_create (BONOBO_OBJECT (control))
		;
	if (corba_object == CORBA_OBJECT_NIL) {
		bonobo_object_unref (BONOBO_OBJECT (control));
		return NULL;
	}
	return gnomera_control_construct (control, corba_object, device);
}
