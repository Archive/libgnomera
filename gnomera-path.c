/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gnomera-path.c - Resolve paths for Gnomera directories and files

   Copyright (C) 2000 Free Software Foundation
   
   Gnomera is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
   
   Gnomera is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   $Id$
*/

#include <gnomera-path.h>

#define DATADIR "/usr/share"
#define LIBDIR "/usr/lib"

gchar *
gnomera_data_path (const gchar *subdir)
{
	return g_strconcat (DATADIR, G_DIR_SEPARATOR_S, subdir, NULL);
}

gchar *
gnomera_library_path (const gchar *subdir)
{
	return g_strconcat (LIBDIR, G_DIR_SEPARATOR_S, subdir, NULL);
}

gchar *
gnomera_configuration_file (const gchar *subfile)
{
	gchar *homedir;
	homedir = g_get_home_dir ();
	if (!homedir) {
		homedir = g_get_tmp_dir ();
	}
	return g_strconcat (homedir, G_DIR_SEPARATOR_S, ".gnomera" G_DIR_SEPARATOR_S,
			    subfile, NULL);
}
