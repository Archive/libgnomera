/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gnomera-response.c - responder device return codes, as defined in spec.
   
   Copyright (C) 2000 Free Software Foundation

   Gnomera is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Gnomera is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   $Id$
*/

#include <config.h>
#include <stdio.h>
#include <glib.h>

/* Temporary hack to markup strings */

#ifdef _
#warning "_ already defined"
#else
#define _(x) x
#endif

#ifdef N_
#warning "N_ already defined"
#else
#define N_(x) x
#endif

#include <gnomera-response.h>

static gchar *response_strings[] = {
/*  	GNOMERA_RESPONSE_UNDEFINED                  = 0x2000, */
	N_("Undefined response"),
	
/*  	GNOMERA_RESPONSE_OK                         = 0x2001, */
	N_("Operation completed successfully."),
	
/*  	GNOMERA_RESPONSE_GENERAL_ERROR              = 0x2002, */
	N_("Operation did not complete.  Unknown error."),
	
/*  	GNOMERA_RESPONSE_SESSION_NOT_OPEN           = 0x2003, */
	N_("Session handle of the operation is not a currently "
	   "open session."),
	
/*  	GNOMERA_RESPONSE_INVALID_TRANSACTION_ID     = 0x2004, */
	N_("The TransactionID is zero or does not refer to a valid "
	   "transaction"),
	
/*  	GNOMERA_RESPONSE_UNSUPPORTED_OPERATION      = 0x2005, */
	N_("The indicated OperationCode appears to be a valid code, "
	   "but the Responser does not support the operation.  "
	   "This error should not normally occur, as the Initator "
	   "should only invoke operations that the Responder indicated "
	   "were supported in its DeviceInfo dataset."),
	
/*  	GNOMERA_RESPONSE_UNSUPPORTED_PARAMETER      = 0x2006, */
	N_("A non-zero parameter was specified in conjunction with the "
	   "operation, and that parameter is not used for that operation.  "
	   "This response is distinctly different from Invalid_Parameter "
	   "as described in Clause 11.3.29"),
	
/*  	GNOMERA_RESPONSE_INCOMPLETE_TRANSFER        = 0x2007, */
	N_("The transfer did not complete.  Any data transferred should "
	   "be discarded.  This response should not be used if the "
	   "transaction was manually cancelled.  "
	   "See Response_Transaction_Cancelled, "
	   "described in Clause 11.3.31."),
	
/*  	GNOMERA_RESPONSE_INVALID_STORAGE_ID         = 0x2008, */
	N_("A StorageID sent with an operation does not refer to an "
	   "actual valid store that is present on the device."
	   "The list of valid StorageIDs should be re-requested, "
	   "along with any appropriate StorageInfo datasets."),
	
/*  	GNOMERA_RESPONSE_INVALID_OBJECT_HANDLE              = 0x2009, */
	N_("An ObjectHandle does not refer to an actual object that "
	   "is present on the device.  The list of valid ObjectHandles "
	   "should be re-requested, along with any appropriate ObjectInfo "
	   "datasets."),
	
/*  	GNOMERA_RESPONSE_UNSUPPORTED_DEVICE_PROP    = 0x200a, */
	N_("The indicated DevicePropCode appears to be a valid code, "
	   "but that property is not supported by the device.  "
	   "This response should not normally occur, as the Initiator "
	   "should only attempt to manipulate properties that the "
	   "Responder indicated were supported in the "
	   "DevicePropertiesSupported array in the DeviceInfo dataset."),
	
/*  	GNOMERA_RESPONSE_INVALID_OBJECT_FORMAT_CODE = 0x200b, */
	N_("Invalid object format code.  The device does not support it."),
	
/*  	GNOMERA_RESPONSE_STORE_FULL                 = 0x200c, */
	N_("The store that the operation referred to is full."),
	
/*  	GNOMERA_RESPONSE_OBJECT_WRITE_PROTECTED     = 0x200d, */
	N_("The object that the operation referred to is write-protected."),
	
/*  	GNOMERA_RESPONSE_STORE_READ_ONLY            = 0x200e, */
	N_("The store that the operation referred to is read-only."),
	
/*  	GNOMERA_RESPONSE_ACCESS_DENIED              = 0x200f, */
	N_("Access to the data referred to by the operation was denied.  "
	   "Access will be continued to be denied, given the current state"
	   "of the device."),
	
/*  	GNOMERA_RESPONSE_NO_THUMBNAIL_PRESENT       = 0x2010, */
	N_("A data object with the specified ObjectHandle exists, "
	   "but the data object does not contain a producible thumbnail."),
	
/*  	GNOMERA_RESPONSE_SELFTEST_FAILED            = 0x2011, */
	N_("The deviced failed an internal self-test."),
	
/*  	GNOMERA_RESPONSE_PARTIAL_DELETION           = 0x2012, */
	N_("A subset of the objects indicated for deletion were "
	   "actually deleted, due to the fact that some were "
	   "write-protected, or that some obhects were on stores "
	   "that are read-only"),
	
/*  	GNOMERA_RESPONSE_STORE_NOT_AVAILABLE        = 0x2013, */
	N_("The store indicated (or the store that contains the "
	   "indicated object) is not physically available."
	   "This can be caused by media ejection.  This response "
	   "shall not be used to indicate that the store is busy, "
	   "as described in Clause 11.3.25."),
	
/*  	GNOMERA_RESPONSE_UNSUPPORTED_SPEC_BY_SPEC   = 0x2014, */
	N_("The operation attempted to specify action only on "
	   "objects of a particular format, and that capability "
	   "is unsupported.  The operation should be re-attempted "
	   "without specifying by format.  Any response of this "
	   "nature infers that any future attempt to specify by "
	   "format with the indicated operation will result in the "
	   "same response."),
	
/*  	GNOMERA_RESPONSE_INVALID_OBJECT_INFO        = 0x2015, */
	N_("The Initator attempted to issue a SendObject operation "
	   "without having previously sent a corresponding SendObjectInfo "
	   "successfully.  The Initiator should successfully complete "
	   "a SendObjectInfo operation before attempting another "
	   "SendObject operation."),
	
/*  	GNOMERA_RESPONSE_INVALID_CODE_FORMAT        = 0x2016, */
	N_("The indicated data code does not have the correct format, "
	   "and is therefore invalid.  This response is used when the "
	   "most-significant nibble of a datacode does not have the "
	   "format required for that type of code."),
	
/*  	GNOMERA_RESPONSE_UNKNOWN_VENDOR_CODE        = 0x2017, */
	N_("The indicated data code has the correct format, but has "
	   "bit 15 set to 1.  Therefore, the code is a vendor-extended "
	   "code, and this device does not know how to handle the indicated "
	   "code.  This response should typically not occur, "
	   "as the supported vendor extensions should be identifiable by "
	   "examination of the VendorExtensionID and VendorExtensionVersion "
	   "fields in the DeviceInfo dataset."),
	
/*  	GNOMERA_RESPONSE_CAPTURE_ALREADY_TERMINATED = 0x2018, */
	N_("An operation is attempting to terminate a capture "
	   "session initated by a preceding InitateOpenCapture "
	   "operation, and that preceding operation has already "
	   "terminated.  This response is only used for the "
	   "TerminateOpenCapture operation, which is only used for "
	   "open-ended captures.  "
	   "Clause 10.4.24 provides a description of "
	   "the TerminateOpenCapture operation."),
	
/*  	GNOMERA_RESPONSE_DEVICE_BUSY                = 0x2019, */
	N_("The device is not currently able to process a request, "
	   "because it, or the specified store, is busy.  The intent "
	   "of this response is to imply that perhaps at a future time, "
	   "the operation should be re-requested.  This response shall "
	   "not be used to indicate that a store is physically unavailable, "
	   "as described in Clause 11.3.19."),
	
/*  	GNOMERA_RESPONSE_INVALID_PARENT_OBJECT      = 0x201a, */
	N_("The indicated object is not of type association, and "
	   "therefore is not a valid ParentObject.  This response is "
	   "not intended to be used for specified ObjectHandles that do "
	   "not refer to valid objects, which is handled by "
	   "Invalid_ObjectHandle response described in Clause 11.3.9."),
	
/*  	GNOMERA_RESPONSE_INVALID_DEVICE_PROP_FORMAT = 0x201b, */
	N_("An attempt was made to set a DeviceProperty, but the "
	   "DevicePropDesc dataset is not the correct size or format."),
	
/*  	GNOMERA_RESPONSE_INVALID_DEVICE_PROP_VALUE  = 0x201c, */
	N_("An attempt was made to set a DeviceProperty to a value "
	   "that is not allowed by the device."),
	
/*  	GNOMERA_RESPONSE_INVALID_PARAMETER          = 0x201d, */
	N_("A parameter was specified in conjunction with the operation, "
	   "and that although a parameter was expected, the value of the "
	   "parameter is not a legal value.  This response is distinctly "
	   "different from Parameter_Not_Supported, "
	   "as described in Clause 11.3.6."),
	
/*  	GNOMERA_RESPONSE_SESSION_ALREADY_OPEN       = 0x201e, */
	N_("This is the response to an OpenSession operation. "
	   "For multisession devices/transports, this response indicates "
	   "that a session with the specified Session ID is already open.  "
	   "For single-session devices/transports, this response indicates "
	   "that a session is open, and must be closed before another session "
	   "can be opened."),
	
/*  	GNOMERA_RESPONSE_TRANSACTION_CANCELLED      = 0x201f, */
	N_("The operation was interrupted due to manual cancellation "
	   "by the opposing device."),
	
/*  	GNOMERA_RESPONSE_UNSUPPORTED_SPECIFICATION_OF_DESTINATION = 0x2020 */
	N_("Response to SendObject operation, the Responder does not "
	   "support the specification of destination by the Initator.  "
	   "This response infers that the Initator should not attempt to "
	   "specify the object destination in any future SendObjectInfo "
	   "operations, as they will also fail with the same response.")
};

const gchar *
gnomera_response_to_string (GnomeraResponse response_code)
{
	return _(response_strings[response_code - 0x2000]);
}
