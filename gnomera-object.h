/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gnomera-object.h - Routines for handling files retrieved from camera.

   Copyright (C) 2000 Free Software Foundation, Inc.

   Gnomera is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   Gnomera is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   $Id$
*/

#ifndef __GNOMERA_OBJECT_H__
#define __GNOMERA_OBJECT_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <gtk/gtk.h>
#include <gnomera-datatypes.h>
#include <gnomera-device.h>

#define GNOMERA_TYPE_OBJECT         (gnomera_object_get_type ())
#define GNOMERA_OBJECT(obj)         (GTK_CHECK_CAST ((obj), GNOMERA_TYPE_OBJECT, GnomeraObject))
#define GNOMERA_OBJECT_CLASS(klass) (GTK_CHECK_CAST ((obj), GNOMERA_TYPE_OBJECT, GnomeraObjectClass))
#define GNOMERA_IS_OBJECT(obj) (GTK_CHECK_TYPE ((obj), GNOMERA_TYPE_OBJECT))
#define GNOMERA_IS_OBJECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOMERA_TYPE_OBJECT))

typedef struct _GnomeraObject      GnomeraObject;
typedef struct _GnomeraObjectClass GnomeraObjectClass;

struct _GnomeraObject {
	GtkObject object;

	guint32 storage_id                : 4;
	guint16 object_format             : 2;
	guint16 protection_status         : 2;
	guint32 object_compressed_size    : 4;
	guint16 thumb_format              : 2;
	guint32 thumb_compressed_size     : 4;
	guint32 thumb_pix_width           : 4;
	guint32 thumb_pix_height          : 4;
	guint32 image_pix_width           : 4;
	guint32 image_pix_height          : 4;
	guint32 image_bit_depth           : 4;
	AssociationCode association_type  : 2;
	AssociationDesc association_desc  : 4;
	guint32 sequence_number           : 4;
	GString filename;
	GString key_words;

	guint16 storage_handle : 4;         /* Parent object */
	guint16 object_handle  : 4;
};

struct _GnomeraObjectClass {
	GtkObjectClass parent_class;
};

GtkType gnomera_object_get_type (void);

#define gnomera_object_ref(f)   gtk_object_ref   (GTK_OBJECT (f))
#define gnomera_object_unref(f) gtk_object_unref (GTK_OBJECT (f))

int
gnomera_object_open_file (GnomeraObject *go, const char *filename);

int
gnomera_object_save_file (GnomeraObject *go, char *buf, size_t size);

int
gnomera_object_fprintf (GnomeraObject *go, const char *fmt, ...);

int
gnomera_object_close_file (GnomeraObject *go);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GNOMERA_OBJECT_H__ */

